package com.rp.currencyconverter.common

import java.math.BigDecimal
import java.math.RoundingMode

class NumberUtils {

    companion object {
        fun formatBigDecimal(bigDecimal: BigDecimal): String =
            bigDecimal.setScale(2, RoundingMode.HALF_UP).toPlainString()
    }
}