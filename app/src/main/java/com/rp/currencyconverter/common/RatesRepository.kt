package com.rp.currencyconverter.common

import com.rp.currencyconverter.model.RatesResponse
import com.rp.currencyconverter.networking.AppApi
import io.reactivex.Observable
import javax.inject.Inject

class RatesRepository @Inject constructor(private val api: AppApi) {

    fun getRates(base: String): Observable<RatesResponse> {
        return api.getRates(base)
    }

}