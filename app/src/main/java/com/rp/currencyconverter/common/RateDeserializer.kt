package com.rp.currencyconverter.common

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.rp.currencyconverter.model.RateList
import com.rp.currencyconverter.model.Rate
import java.lang.reflect.Type
import java.math.BigDecimal


class RateDeserializer : JsonDeserializer<RateList> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): RateList? {

        val list = RateList()

        for (entry in json!!.asJsonObject.entrySet()) {
            val code: String = entry.key
            val rate = BigDecimal(entry.value.asString)
            list.add(Rate(code, rate))
        }

        return list
    }


}