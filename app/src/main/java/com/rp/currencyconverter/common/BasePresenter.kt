package com.rp.currencyconverter.common

import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<T> {

    protected val subscriptions = CompositeDisposable()

    var view: T? = null
        set(value) {
            field = value
            if (view == null) {
                subscriptions.clear()
            }
        }

    fun hasView(): Boolean {
        return this.view != null
    }

}
