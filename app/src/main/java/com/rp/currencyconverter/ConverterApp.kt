package com.rp.currencyconverter

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.rp.currencyconverter.di.component.AppComponent
import com.rp.currencyconverter.di.component.DaggerAppComponent
import com.rp.currencyconverter.di.module.AppModule

class ConverterApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
        initDagger()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
        appComponent.inject(this)
    }
}