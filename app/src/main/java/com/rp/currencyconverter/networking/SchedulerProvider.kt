package com.rp.currencyconverter.networking

import io.reactivex.ObservableTransformer

interface SchedulerProvider {

    fun <T> applySchedulers(): ObservableTransformer<T, T>

}