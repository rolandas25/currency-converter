package com.rp.currencyconverter.networking

import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors

class DefaultSchedulerProvider : SchedulerProvider {

    private val appScheduler = Schedulers.from(Executors.newSingleThreadExecutor())

    override fun <T> applySchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer { observable ->
            observable.subscribeOn(appScheduler)
                .observeOn(AndroidSchedulers.mainThread())
        }
    }
}