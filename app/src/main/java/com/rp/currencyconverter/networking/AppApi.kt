package com.rp.currencyconverter.networking

import com.rp.currencyconverter.model.RatesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface AppApi {

    @GET("latest")
    fun getRates(@Query("base") base: String): Observable<RatesResponse>

}