package com.rp.currencyconverter.di.component

import com.rp.currencyconverter.converter.ConverterActivity
import com.rp.currencyconverter.di.scope.PerView
import dagger.Component

@PerView
@Component(dependencies = [AppComponent::class])
interface RatesComponent {

    @Component.Factory
    interface Factory {
        fun create(appComponent: AppComponent): RatesComponent
    }

    fun inject(app: ConverterActivity)

}