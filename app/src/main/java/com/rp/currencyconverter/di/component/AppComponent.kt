package com.rp.currencyconverter.di.component

import com.rp.currencyconverter.ConverterApp
import com.rp.currencyconverter.di.module.AppModule
import com.rp.currencyconverter.networking.AppApi
import com.rp.currencyconverter.networking.SchedulerProvider
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun getApi(): AppApi

    fun getSchedulerProvider(): SchedulerProvider

    fun inject(app: ConverterApp)

}