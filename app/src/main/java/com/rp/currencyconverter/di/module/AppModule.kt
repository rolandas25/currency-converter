package com.rp.currencyconverter.di.module

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rp.currencyconverter.common.RateDeserializer
import com.rp.currencyconverter.model.RateList
import com.rp.currencyconverter.networking.AppApi
import com.rp.currencyconverter.networking.DefaultSchedulerProvider
import com.rp.currencyconverter.networking.SchedulerProvider
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class AppModule(private val app: Application) {

    companion object {
        const val API_URL = "https://revolut.duckdns.org"
    }

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideSchedulerProvider(): SchedulerProvider = DefaultSchedulerProvider()

    @Provides
    @Singleton
    fun provideApi(gson: Gson): AppApi {
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(
                RxJava2CallAdapterFactory.create()
            )
            .addConverterFactory(
                GsonConverterFactory.create(gson)
            )
            .baseUrl(API_URL)
            .build()

        return retrofit.create(AppApi::class.java)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder()
            .registerTypeAdapter(RateList::class.java, RateDeserializer())
            .create()
    }
}