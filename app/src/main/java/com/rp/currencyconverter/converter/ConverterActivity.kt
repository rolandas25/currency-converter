package com.rp.currencyconverter.converter

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.rp.currencyconverter.ConverterApp
import com.rp.currencyconverter.CurrenciesAdapter
import com.rp.currencyconverter.R
import com.rp.currencyconverter.di.component.DaggerRatesComponent
import com.rp.currencyconverter.model.RateViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigDecimal
import javax.inject.Inject


class ConverterActivity : AppCompatActivity(), ConverterView {

    @Inject
    lateinit var presenter: ConverterPresenter

    private var adapter: CurrenciesAdapter = CurrenciesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val appComponent = (applicationContext as ConverterApp).appComponent
        DaggerRatesComponent.factory().create(appComponent).inject(this)

        adapter.currenciesListener = presenter
        adapter.setHasStableIds(true)

        currenciesRecyclerView.layoutManager = LinearLayoutManager(this)
        currenciesRecyclerView.setHasFixedSize(true)
        currenciesRecyclerView.adapter = adapter
        (currenciesRecyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
    }

    override fun onStart() {
        super.onStart()
        presenter.view = this
        presenter.load("EUR", BigDecimal("100"))
    }

    override fun onStop() {
        super.onStop()
        presenter.view = null
    }

    override fun showRates(viewModelList: List<RateViewModel>) {
        adapter.viewModelList = viewModelList.toMutableList()
    }

    override fun notifyItemRangeChanged(start: Int, itemCount: Int) {
        currenciesRecyclerView.post {
            adapter.notifyItemRangeChanged(start, itemCount)
        }
    }

    override fun notifyDataSetChanged() {
        currenciesRecyclerView.post {
            adapter.notifyDataSetChanged()
        }
    }

    override fun scrollToTop() {
        val firstVisiblePosition =
            (currenciesRecyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
        if (firstVisiblePosition != 0) {
            currenciesRecyclerView.smoothScrollToPosition(0)
        }
    }

    override fun showError() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show()
    }
}
