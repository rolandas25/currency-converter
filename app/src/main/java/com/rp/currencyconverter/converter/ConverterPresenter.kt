package com.rp.currencyconverter.converter

import com.neovisionaries.i18n.CurrencyCode
import com.rp.currencyconverter.CurrenciesAdapter
import com.rp.currencyconverter.common.BasePresenter
import com.rp.currencyconverter.common.NumberUtils
import com.rp.currencyconverter.common.RatesRepository
import com.rp.currencyconverter.model.RateViewModel
import com.rp.currencyconverter.model.RatesResponse
import com.rp.currencyconverter.networking.SchedulerProvider
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class ConverterPresenter @Inject constructor(
    private val ratesRepository: RatesRepository,
    private val schedulerProvider: SchedulerProvider
) :
    BasePresenter<ConverterView>(), CurrenciesAdapter.CurrenciesListener {

    private var currentCurrency: String? = null
    private var currentRate: BigDecimal? = null
    private var scrollToTop: Boolean = false
    private var updating: Boolean = false

    override fun onInputChange(position: Int, newCurrencyCode: String, newRate: BigDecimal?) {
        if (currentCurrency == newCurrencyCode && currentRate == newRate) {
            return
        }
        scrollToTop = true
        updating = false
        load(newCurrencyCode, newRate)
    }

    fun load(base: String, input: BigDecimal?) {
        val notifyAll: Boolean = currentCurrency != base
        currentCurrency = base
        currentRate = input
        subscriptions.clear()
        subscriptions.add(
            ratesRepository.getRates(base)
                .repeatWhen {
                    it.delay(1, TimeUnit.SECONDS)
                }
                .compose(schedulerProvider.applySchedulers())
                .subscribe(
                    {
                        val viewModelList: MutableList<RateViewModel> =
                            prepareViewModelList(base, input, it)
                        view?.showRates(viewModelList)
                        if (notifyAll && !updating) {
                            view?.notifyDataSetChanged()
                        } else {
                            view?.notifyItemRangeChanged(1, viewModelList.size - 1)
                        }
                        if (scrollToTop) {
                            view?.scrollToTop()
                            scrollToTop = false
                        }
                        updating = true
                    }
                    ,
                    {
                        it.printStackTrace()
                        view?.showError()
                    }
                ))

    }

    private fun prepareViewModelList(
        base: String,
        input: BigDecimal?,
        ratesResponse: RatesResponse
    ): MutableList<RateViewModel> {
        val viewModelList: MutableList<RateViewModel> = mutableListOf()
        val baseRateViewModel = RateViewModel(
            base,
            getCurrencyDisplayName(base),
            input?.toPlainString() ?: ""
        )
        viewModelList.add(baseRateViewModel)

        for (rate in ratesResponse.rates) {
            val rateViewModel = RateViewModel(
                rate.currencyCode,
                getCurrencyDisplayName(rate.currencyCode),
                convert(input, rate.value)
            )
            viewModelList.add(rateViewModel)
        }
        return viewModelList
    }

    private fun convert(
        value: BigDecimal?,
        rate: BigDecimal
    ) = value?.let { NumberUtils.formatBigDecimal(value.multiply(rate)) } ?: ""

    private fun getCurrencyDisplayName(code: String): String {
        return CurrencyCode.getByCode(code).getName()
    }

}