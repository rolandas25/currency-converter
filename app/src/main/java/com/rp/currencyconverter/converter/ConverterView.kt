package com.rp.currencyconverter.converter

import com.rp.currencyconverter.model.RateViewModel

interface ConverterView {

    fun showRates(viewModelList: List<RateViewModel>)
    fun notifyItemRangeChanged(start: Int, itemCount: Int)
    fun notifyDataSetChanged()
    fun showError()
    fun scrollToTop()
}