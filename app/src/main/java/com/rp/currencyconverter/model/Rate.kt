package com.rp.currencyconverter.model

import java.math.BigDecimal

class Rate(
    val currencyCode: String,
    val value: BigDecimal
)