package com.rp.currencyconverter.model

data class RateViewModel(val code: String, val title: String, val rate: String)