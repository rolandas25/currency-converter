package com.rp.currencyconverter.model

import java.util.*

data class RatesResponse(
    var base: String = "",
    var date: Date = Date(),
    var rates: RateList = RateList()
)