package com.rp.currencyconverter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding3.widget.textChangeEvents
import com.rp.currencyconverter.model.RateViewModel
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.currency_list_item.view.*
import java.math.BigDecimal
import kotlin.math.min

class CurrenciesAdapter : RecyclerView.Adapter<CurrenciesAdapter.ViewHolder>() {

    var viewModelList: MutableList<RateViewModel> = mutableListOf()
    lateinit var currenciesListener: CurrenciesListener

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rateViewModel: RateViewModel = viewModelList[position]
        holder.currencyCode.text = rateViewModel.code
        holder.currencyTitle.text = rateViewModel.title

        if (!holder.currencyRate.isFocused && position == 0) {
            holder.currencyRate.isFocusableInTouchMode = true
            holder.currencyRate.requestFocus()
        }

        if (holder.currencyRate.isFocused) {
            val selection = holder.currencyRate.selectionStart
            holder.currencyRate.setText(rateViewModel.rate)
            holder.currencyRate.setSelection(min(selection, rateViewModel.rate.length))
        } else {
            holder.currencyRate.setText(rateViewModel.rate)
        }

        holder.currencyFlag.setActualImageResource(
            getCurrencyFlagRes(
                holder.currencyFlag.context,
                rateViewModel.code
            )
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val holder = ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.currency_list_item,
                parent,
                false
            )
        )

        holder.currencyRate.setOnClickListener {
            holder.currencyRate.isFocusableInTouchMode = true
            holder.currencyRate.requestFocus()

            notifyAboutChange(holder)
        }

        holder.itemView.setOnClickListener {
            holder.currencyRate.isFocusableInTouchMode = true
            holder.currencyRate.requestFocus()

            notifyAboutChange(holder)
        }

        holder.currencyRate.setOnFocusChangeListener { _, focused ->
            if (focused) {
                holder.textChangesDisposable = holder.currencyRate
                    .textChangeEvents()
                    .skipInitialValue()
                    .subscribe {
                        if (holder.currencyRate.isFocused) {
                            notifyAboutChange(holder)
                        }
                    }
            } else {
                holder.currencyRate.isFocusableInTouchMode = false
                holder.textChangesDisposable?.dispose()
            }
        }


        return holder
    }

    private fun notifyAboutChange(holder: ViewHolder) {
        var input: BigDecimal? = null
        if (holder.currencyRate.text.isNotEmpty()) {
            input = BigDecimal(holder.currencyRate.text.toString())
        }

        val position = holder.adapterPosition
        if (position != RecyclerView.NO_POSITION) {
            val rateViewModel = viewModelList[position]
            currenciesListener.onInputChange(position, rateViewModel.code, input)
        }
    }

    override fun getItemId(position: Int): Long {
        return viewModelList[position].code.hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return viewModelList.size
    }

    @SuppressLint("DefaultLocale")
    private fun getCurrencyFlagRes(context: Context, code: String): Int {
        var flagIdentifier = code.toLowerCase()
        if (flagIdentifier == "try") {
            flagIdentifier = "try_"
        }
        return context.resources.getIdentifier(flagIdentifier, "drawable", context.packageName)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var textChangesDisposable: Disposable? = null

        val currencyFlag = view.currencyFlag!!
        val currencyCode = view.currencyCode!!
        val currencyTitle = view.currencyTitle!!
        val currencyRate = view.currencyRate!!
    }

    interface CurrenciesListener {
        fun onInputChange(position: Int, newCurrencyCode: String, newRate: BigDecimal?)
    }
}