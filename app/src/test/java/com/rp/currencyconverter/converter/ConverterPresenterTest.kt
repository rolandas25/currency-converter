package com.rp.currencyconverter.converter

import com.rp.currencyconverter.TestSchedulerRule
import com.rp.currencyconverter.common.RatesRepository
import com.rp.currencyconverter.model.Rate
import com.rp.currencyconverter.model.RatesResponse
import com.rp.currencyconverter.networking.SchedulerProvider
import io.mockk.*
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class ConverterPresenterTest {

    @get:Rule
    var testSchedulerRule = TestSchedulerRule()

    private lateinit var converterPresenter: ConverterPresenter
    private lateinit var view: ConverterView
    private lateinit var ratesRepository: RatesRepository
    private lateinit var schedulerProvider: SchedulerProvider

    @Before
    fun setUp() {
        view = spyk()
        ratesRepository = mockk()
        schedulerProvider = object : SchedulerProvider {
            override fun <T> applySchedulers(): ObservableTransformer<T, T> {
                return ObservableTransformer { observable ->
                    observable
                }
            }
        }

        converterPresenter = ConverterPresenter(ratesRepository, schedulerProvider)
        converterPresenter.view = view
    }

    @Test
    fun onInputChange() {

        mockRates()

        converterPresenter.onInputChange(0, "EUR", BigDecimal(3))

        verify(exactly = 1) { view.scrollToTop() }
    }

    @Test
    fun onInputChange_whenAlreadyCalled() {

        mockRates()
        converterPresenter.onInputChange(0, "EUR", BigDecimal(3))
        verify(exactly = 1) {
            view.showRates(any())
            view.notifyDataSetChanged()
        }

        clearMocks(view)
        converterPresenter.onInputChange(0, "EUR", BigDecimal(3))
        verify(exactly = 0) {
            view.showRates(any())
        }
    }

    @Test
    fun onInputChange_whenInputChangedTwoTimes() {

        mockRates()
        converterPresenter.onInputChange(0, "EUR", BigDecimal(3))
        verify(exactly = 1) { view.showRates(any()) }

        clearMocks(view)
        converterPresenter.onInputChange(0, "EUR", BigDecimal(4))
        verify(exactly = 1) {
            view.showRates(any())
            view.notifyItemRangeChanged(1, 1)
        }
    }

    @Test
    fun load() {

        mockRates()

        converterPresenter.load("EUR", BigDecimal(2))

        verify(exactly = 1) { view.showRates(any()) }

        verify {
            view.showRates(match {
                val baseRateMatch = it[0].code == "EUR" && it[0].rate == "2"
                var calculationMatch = false
                for (rateViewModel in it) {
                    if (rateViewModel.code == "USD" && rateViewModel.rate == "2.44") {
                        calculationMatch = true
                    }
                }
                baseRateMatch && calculationMatch
            })
        }
    }

    @Test
    fun load_whenRepeating() {

        mockRates()

        converterPresenter.load("EUR", BigDecimal(2))

        testSchedulerRule.testScheduler.advanceTimeBy(5, TimeUnit.SECONDS)

        verify(exactly = 6) { view.showRates(any()) }
    }

    @Test
    fun load_whenError() {

        every { ratesRepository.getRates("EUR") } returns Observable.error(RuntimeException())

        converterPresenter.load("EUR", BigDecimal(2))

        verify { view.showError() }
        verify(exactly = 0) { view.showRates(any()) }
    }

    private fun mockRates() {
        val rate = Rate("USD", BigDecimal("1.22"))
        val ratesResponse = RatesResponse()
        ratesResponse.base = "EUR"
        ratesResponse.rates.add(rate)
        every { ratesRepository.getRates("EUR") } returns Observable.just(ratesResponse)
    }


}