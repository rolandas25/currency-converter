package com.rp.currencyconverter.common

import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

class NumberUtilsTest {

    @Test
    fun format() {
        assertEquals("0.00", NumberUtils.formatBigDecimal(BigDecimal.ZERO))
        assertEquals("1.00", NumberUtils.formatBigDecimal(BigDecimal.ONE))
        assertEquals("1.22", NumberUtils.formatBigDecimal(BigDecimal(1.222)))
        assertEquals("2.20", NumberUtils.formatBigDecimal(BigDecimal(2.2)))
    }

}