package com.rp.currencyconverter.common

import com.google.gson.JsonDeserializer
import com.google.gson.JsonParser
import com.rp.currencyconverter.model.RateList
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class RateDeserializerTest {

    private lateinit var deserializer: JsonDeserializer<RateList>

    @Before
    fun setUp() {
        deserializer = RateDeserializer()
    }

    @Test
    fun deserialize_withData() {

        val parser = JsonParser()
        val testObject = parser.parse(
            "{\n" +
                    "    \"AUD\": 1.6137,\n" +
                    "    \"BGN\": 1.9525,\n" +
                    "    \"BRL\": 4.7837,\n" +
                    "    \"CAD\": 1.5312,\n" +
                    "    \"CHF\": 1.1256,\n" +
                    "    \"CNY\": 7.9317,\n" +
                    "    \"CZK\": 25.671,\n" +
                    "    \"DKK\": 7.4441,\n" +
                    "    \"GBP\": 0.89672,\n" +
                    "    \"HKD\": 9.1169,\n" +
                    "    \"HRK\": 7.4215,\n" +
                    "    \"HUF\": 325.94,\n" +
                    "    \"IDR\": 17294,\n" +
                    "    \"ILS\": 4.1635,\n" +
                    "    \"INR\": 83.576,\n" +
                    "    \"ISK\": 127.58,\n" +
                    "    \"JPY\": 129.33,\n" +
                    "    \"KRW\": 1302.6,\n" +
                    "    \"MXN\": 22.327,\n" +
                    "    \"MYR\": 4.8039,\n" +
                    "    \"NOK\": 9.7595,\n" +
                    "    \"NZD\": 1.7603,\n" +
                    "    \"PHP\": 62.486,\n" +
                    "    \"PLN\": 4.311,\n" +
                    "    \"RON\": 4.6306,\n" +
                    "    \"RUB\": 79.44,\n" +
                    "    \"SEK\": 10.573,\n" +
                    "    \"SGD\": 1.5973,\n" +
                    "    \"THB\": 38.065,\n" +
                    "    \"TRY\": 7.6153,\n" +
                    "    \"USD\": 1.1614,\n" +
                    "    \"ZAR\": 17.793\n" +
                    "  }"
        ).asJsonObject

        val rate = deserializer.deserialize(testObject, null, null)

        assertEquals("AUD", rate[0].currencyCode)
        assertEquals("1.6137", rate[0].value.toString())
    }

    @Test
    fun deserialize_whenObjectEmpty() {
        val parser = JsonParser()
        val testObject = parser.parse("{}").asJsonObject
        val rateList = deserializer.deserialize(testObject, null, null)
        assertEquals(0, rateList.size)
    }

}